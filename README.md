# OpenBSD port of libmatthew
### Developed on and tested for 6.5-stable and 6.5-current #15

**Dependencies**

* gmake
* gcc (8.3)
* jdk (1.8)

**How-to**

```
export PATH=/usr/local/jdk-1.8.0/bin:/usr/local/jdk-1.8.0/jre/bin:$PATH
export JAVA_HOME=/usr/local/jdk-1.8.0
gmake
```